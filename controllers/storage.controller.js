const storageCtrl = {}
const StorageInspeccion =  require('../models/storage_inspeccion')
const mongoose = require("mongoose")

storageCtrl.getAll = async (req, res ) => {
   const storage = await StorageInspeccion.find()
   res.json(storage)
}

storageCtrl.getById = async (req, res ) => {
   const storage = await StorageInspeccion.find({ _id: req.params.id })
   if(storage)
     res.json(storage)
   else 
     res.json({ status: false })
}


storageCtrl.save = async (req, res ) => {
    for(let i = 0; i < req.body.length; i++) 
    {
     const newStorage = new StorageInspeccion({
        vehiculo: mongoose.Types.ObjectId(req.body[i].idplates),
        category :  req.body[i].category,
        observation: req.body[i].observation
     })
     await newStorage.save()
    }
    res.json({status:true})
}

module.exports = storageCtrl