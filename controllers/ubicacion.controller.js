const ubicacionCtrl = {}

const Ubicacion =  require('../models/ubicacion')

ubicacionCtrl.create = async (req, res ) => {
  const sedes = new Ubicacion
  ({
    url : req.body.url,
    empresa : req.body.empresa.trim()
  })
  await sedes.save()    
  res.json({
    status: true
  })
}

module.exports = ubicacionCtrl