const vechicleCtrl = {};

const mongoose = require("mongoose");
const CvVehicle = require("../models/cv_vehicle");
const Driver = require("../models/driver");
const initSendEmail = require("../middlewares/mail");


vechicleCtrl.getVehicles = async (req, res) => {
  const cvVehicles = await CvVehicle.find({ status: true }) 
  res.json(cvVehicles)
};

vechicleCtrl.getVehiclesPlacas = async (req, res) => {
  const placas = await CvVehicle.find({ status: true }).select("-__v -names -surnames -documentType -documentNumber -address -phone -email -gender -status -role -created_at -modeloplate -modeloplatetracker -city -colorplate -colorplatetracker -kilometraje") 
  res.json(placas)
};


vechicleCtrl.getVehicle = async (req, res) => {
  const cvVehicle = await CvVehicle.findById(req.params.id);
  res.json(cvVehicle)
}

vechicleCtrl.getVehiclePlatesAndDriver = async (req, res) => {
  const cvVehicle = await CvVehicle.findOne({ driver:req.params.id })
  if(cvVehicle){
    res.json(cvVehicle)
  }else
    res.json({ status: false })
}

vechicleCtrl.searchVehicle = async (req, res) => {
  const cvVehicle = await CvVehicle.find({plate: { $regex: ".*" + req.body.name + ".*" },
    status: true
  })
  res.json( cvVehicle )
}

vechicleCtrl.validatePlate = async (req, res) => {
  const cvVehicle = await CvVehicle.find({ plate: req.body.plate , status: true })
  if (cvVehicle) 
  {
    res.status(200).send({ status: true })
  }
  else 
  {
    res.status(200).send({ status: false })
  }
}

vechicleCtrl.createVehicleWithPropietaryWeb = async (req, res) => {
  const vehicle = new CvVehicle
  ({
    plate : req.body.vehiculo.placavehiculo,
    platetracker: req.body.vehiculo.placatrailer,
    modeloplate : req.body.vehiculo.modeloplate,
    modeloplatetracker : req.body.vehiculo.modeloplatetracker,
    colorplate : req.body.vehiculo.colorplate,
    colorplatetracker : req.body.vehiculo.colorplatetracker,
    kilometraje: req.body.vehiculo.kilometraje,
    tipevehicle : req.body.vehiculo.tipovehiculo,
    names : req.body.propietario.nombre,
    surnames : req.body.propietario.apellidos,
    documentType : mongoose.Types.ObjectId(req.body.propietario.tipoID),
    documentNumber : req.body.propietario.numID,
    address: req.body.propietario.direccion,
    phone:  req.body.propietario.telefono,
    email : req.body.propietario.correo,
    gender: mongoose.Types.ObjectId(req.body.propietario.genero),
    city: req.body.propietario.ciudad,
    role : mongoose.Types.ObjectId("5d55db01e7179a084eef5110"),
    driver: mongoose.Types.ObjectId(req.body.propietario.driver)
  })
  await vehicle.save( (err, vehicle )=>{
    if (err) return res.json({ status:"duplicado", msg: "Se ha intentado duplicar registros, por favor verifica la información "})
    initSendEmail.notificationRegisterPropietario(req.body.propietario.correo)
    res.json({ status: true, vehicle:vehicle })
  })
}

vechicleCtrl.createVehicleWithPropietary = async (req, res) => {
  const driver = new Driver
  ({
    names : req.body.nombrecd,
    surnames : req.body.apellidoscd,
    documentType : mongoose.Types.ObjectId(req.body.tipoIDcd),
    documentNumber : req.body.numIDcd,
    address: req.body.direccioncd,
    phone:  req.body.telefonocd,
    email : req.body.correocd,
    city: req.body.ciudadcd,
    gender: mongoose.Types.ObjectId(req.body.generocd),
    role : mongoose.Types.ObjectId("5ca579e80f91700d0b03cd05")
  })
  await driver.save( (err, driver )=>{
    if (err) return res.json({ status:"duplicado", msg: "Se ha intentado duplicar registros, por favor verifica la información "})
    initSendEmail.notificationDocsDriver(req.body.correocd)
    res.json({ status: true, driver:driver._id })
  })
}

vechicleCtrl.updateVehicleWithPropietary = async (req, res) => {
  const vehicle =
  ({
    plate : req.body.plate,
    tipevehicle : req.body.tipevehicle,
    names : req.body.names,
    surnames : req.body.surnames,
    documentType : mongoose.Types.ObjectId(req.body.documentType),
    documentNumber : req.body.documentNumber,
    address: req.body.address,
    phone:  req.body.phone,
    email : req.body.email,
    gender: mongoose.Types.ObjectId(req.body.gender)
  })
  await CvVehicle.findByIdAndUpdate(req.body.id, { $set: vehicle }, { new: true })
  res.json({
    status: "Vehiculo actualizado"
  });
};

vechicleCtrl.deleteVehicleWithPropietary = async (req, res) => {
  const vehicle = 
  {
    status: false
  }
  await CvVehicle.findByIdAndUpdate(req.params.id, { $set: vehicle })
  res.json({
    status: "Vehiculo eliminado"
  });
};

module.exports = vechicleCtrl;
