const companyCtrl = {}

const mongoose = require("mongoose")
const Company =  require('../models/company')
const initData = require("../models/initData")
const initSendEmail = require("../middlewares/mail");


companyCtrl.getCompanys = async (req, res ) => {
    const company = await Company.find()
    res.json(company)   
}

companyCtrl.getCompany = async (req, res ) => {
    const company = await Company.findById(req.params.id).select("-ccomercio -rutname -contacto -city -__v")
    res.json(company)
}

companyCtrl.getCompanyByCity = async (req, res ) => {
    const city = req.params.idcity
    const company = await Company.find({city:city})
    res.json(company);
}

companyCtrl.searchCompany = async (req, res ) => {
    const company = await Company.find({'name': { $regex: '.*' + req.body.name + '.*' } })
    if( company )
    {
      res.status(200).send({ company , status : true })
    }
    else
    {
        res.json({
            status: 'Empresa no encontrada'
        })
    }
}

//fala por terminar
companyCtrl.validateEmail = async (req, res ) => {
    const company = await Company.find({'name': { $regex: '.*' + req.body.name + '.*' } })
    if( company )
    {
      res.status(200).send({ company , status : true })
    }
    else
    {
        res.json({
            status: 'Empresa no encontrada'
        })
    }
} 

companyCtrl.createCompany = async (req, res ) => {
    const company = new Company({
        ccomercio:req.body.ccomercio,
        rutname:req.body.rutname,
        representantelegal: req.body.representante,
        celular : req.body.celular,
        telefono : req.body.telefono,
        direccion: req.body.direccion,
        contacto : req.body.contacto,
        nit: req.body.nit,
        digito: req.body.digito,
        businessname: req.body.businessname,
        img : req.body.img,
        correo: req.body.correo,
        dpt :req.body.dpt,
        city :req.body.city,
        role :mongoose.Types.ObjectId('5ca579e80f91700d0b03cd06')
    })
    
    const resultCompany = await company.save()
    initData.createUserCompany(req.body.correo, req.body.nit, resultCompany._id)
    initSendEmail.sendSubscription(req.body.correo, resultCompany._id)
    res.json({
      status: 'Empresa registrada'
    })
}

companyCtrl.updateCompany = async (req, res ) => {
   const { id } = req.params
   const company = {
        status : false
   }
   await Company.findByIdAndUpdate(id, {$set: company })
   res.json({
       status: 'Empresa actualizada'
   })
 }

companyCtrl.deleteCompany = async (req, res) => {
    const company = {
        status : false 
    }
    await Company.findByIdAndUpdate(req.params.id, { $set: company })
    res.json({
        status: 'Empresa eliminada'
    })
}

module.exports = companyCtrl