const confirmationCtrl = {}
const Company = require('../models/company')
const TokenModel = require('../models/token')

confirmationCtrl.enableAccountCompany = async (req, res ) => {
   const token = await TokenModel.findOne( { token : req.params.id } )
   if(token)
   {
     Company.findOne( { _id: token.company }, function(err, response){
      if (err) return handleError(err);
      response.status = true
      response.save()
      res.json({ status: true })
    })
  }
  else
  {
    res.json({ status: false })
  }
}

confirmationCtrl.getTipoVehiculo = async (req, res ) => {
    const tipoVehiculo = await TipoVehiculo.find( { status: true } )
    res.json(tipoVehiculo)
}

confirmationCtrl.getCity = async (req, res ) => {
    const city = await City.findById(req.params.id)
    res.json(city)
}

module.exports = confirmationCtrl