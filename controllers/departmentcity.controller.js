const departamentcityCtrl = {}

const DepartamentCity =  require('../models/departmentsCitys')

departamentcityCtrl.getAll = async (req, res ) => {
  const dpts = await DepartamentCity.find()
  res.json(dpts)
}
module.exports = departamentcityCtrl