const refempresrialCtrl = {};

const mongoose = require("mongoose");
const ReferenciaEmpresarial = require("../models/referenciaempresarial");


refempresrialCtrl.search = async (req, res) => {
  const reff = await ReferenciaEmpresarial.findById(req.body._id)
  res.json(reff)
};

refempresrialCtrl.getByDriver = async (req, res) => {
  const refe = await ReferenciaEmpresarial.find({ refcompany:req.params.id} )
  if(refe)
   res.json(refe)
  else
  res.json({ status: false })
};

refempresrialCtrl.create = async (req, res) => {
  const refempresarial = new ReferenciaEmpresarial({
    refcompany: mongoose.Types.ObjectId(req.body.idconductorlb),
    names : req.body.nombrelb ,
    surnames : req.body.apellidoslb,
    address: req.body.direccionlb,
    phone:  req.body.telefonolb ,
    city : req.body.ciudadlb,
    email : req.body.correolb,
    confirmacion: req.body.confirmacionlb
  })
  await refempresarial.save()
  res.json({
    status: true
  })
};



module.exports = refempresrialCtrl
