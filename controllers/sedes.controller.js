const sedesCtrl = {}

const Sedes =  require('../models/sedes')

sedesCtrl.createSede = async (req, res ) => {
    var i = 0
    if(req.body.empresa != null)
    {
      for(let i = 0; i < req.body.cnt.length; i++) {
        while(i < req.body.cnt ) 
        {   
          const sedes = new Sedes({
            nombre : "",
            celular : "",
            ciudad : "",
            departamento: "",
            empresa : req.body.empresa
          })
          await sedes.save()    
          i++;
        }
     } 
     res.json({
        status: true
      })
    }
    else
    {
      res.json({
        status: false
      })
    }
    
}

sedesCtrl.getSedes = async (req, res ) => {
   const sedes = await Sedes.find({ empresa:req.body.empresa }).select("nombre celular ciudad empresa _id")
  res.json(sedes)
}

sedesCtrl.saveSedes = async (req, res ) => {
  for (let i = 0; i < req.body.length; i++) {
    const sedes = {
      nombre: req.body[i].nombre,
      celular: req.body[i].celular,
      ciudad: req.body[i].ciudad,
      empresa: req.body[i].empresa
    };
    await Sedes.findByIdAndUpdate(req.body[i]._id, { $set: sedes }, { new: false })
   }
   res.json({ status: true })
}

module.exports = sedesCtrl