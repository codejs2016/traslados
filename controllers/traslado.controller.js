const trasladoCtrl = {};
const Traslado = require("../models/traslado");
const Producto = require("../models/product");
const moment = require('moment')

trasladoCtrl.get = async (req, res) => {
  const traslado = await Traslado.find()
  res.json(traslado)
};

trasladoCtrl.getByPunto = async (req, res) => {
  const traslado = await Traslado.find({puntoventa:req.body.name, status: true })
  res.json(traslado)
};


trasladoCtrl.validateStock = async (req, res) => {
  const producto = await Producto.findById(req.params.id)
  res.json(producto)
};

trasladoCtrl.search = async (req, res) => {
  const producto = await Traslado.find()
  res.json(producto)
};

trasladoCtrl.searchTraslado = async (req, res) => {
   const traslado = await Traslado.find({
    producto: { $regex: ".*" +req.body.name.toLowerCase() +".*" },
    status: true
   }); 
  
  if (traslado) {
    res.status(200).send({ traslado, status: true });
  } else {
    res.json({
      status: false
    });
  }
};

trasladoCtrl.searchTrasladoByFecha = async (req, res) => {
  let fechanew = moment(req.body.fecha).format("YYYY-MM-DD"); 
  const traslado = await Traslado.find({ created_at: fechanew })
  if (traslado) {
    res.status(200).send({ traslado, status: true });
  } else {
    res.json({
      status: false
    });
  }
};


trasladoCtrl.create = async (req, res) => {
   let fecha = new Date();
   let fechaSave = moment(fecha).format("YYYY-MM-DD"); 
   let hora = fecha.getHours()+":"+fecha.getMinutes()+":"+fecha.getSeconds();
 
   for (let i = 0; i < req.body.data.length; i++) {
    const traslado = new Traslado({
      puntoventa : req.body.idpunto ,
      producto : req.body.data[i].name ,
      referencia: req.body.data[i].ref,
      ctg : req.body.data[i].ctg ,
      cantidad: req.body.data[i].cnt,
      responsable: req.body.responsable,
      created_at:fechaSave,
      hora: hora
    })
    traslado.save()
     
    await Producto.findOne( { name: req.body.data[i].name }, function(err, response){
      if (err) return handleError(err)
      response.stock =  response.stock - req.body.data[i].cnt
      response.save()
    })
  }
  res.json({
    status: true
  });
};

module.exports = trasladoCtrl
