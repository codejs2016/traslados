const utilCtrl = {}

const CvVehicle = require("../models/cv_vehicle");

utilCtrl.existPlate = async (req, res ) => {
    let validate = null
    if(req.body.opc == 1){
        validate = 
        {
         plate : req.body.plate  
        }
    }
    else
    {
        validate = 
        {
          platetracker : req.body.plate  
        }
    }
    const existplate = await CvVehicle.find(validate)
    .select("-tipevehicle -names -surnames -documentType -documentNumber -address -phone -email -gender -city -status -role -created_at ")
    if(existplate)
     res.json({status: true})
    else
     res.json({status: false })
}

module.exports = utilCtrl