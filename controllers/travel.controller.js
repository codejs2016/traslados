const travelCtrl = {};

const mongoose = require("mongoose");
const Travel = require("../models/travel");
const Company = require("../models/company")


travelCtrl.getTravels = async (req, res) => {
  const travels = await Travel.find({ status: true })
  res.json(travels)
  //  Travel.find({ status: true })
  //   .populate('company')
  //   .exec(function (err, results) {
  //       if (err) return handleError(err);
  //       res.json( results )
  //  });
};

travelCtrl.search = async (req, res) => {
  const travels = await Travel.findById(req.body._id)
  res.json(travels)
};

travelCtrl.getTravelsByCompany = async (req, res) => {
  console.log(  req.params.id )
  const travels = await Travel.find({
    company: req.params.id,
    status: true
  });
  res.json(travels)
}

travelCtrl.createTravel = async (req, res) => {
  const travel = new Travel({
    company: mongoose.Types.ObjectId(req.body.idcompany),
    cityOrigen: req.body.origen,
    cityDestination: req.body.destino,
    port: req.body.puerto,
    bodega: req.body.bodega,
    typelaod: req.body.typocarga,
    vigencia: req.body.vigencia,
    typevhicle: req.body.tipovehiculo,
    weight: req.body.peso,
    flete: req.body.flete,
    updated_at: new Date()
  })
  await travel.save()
  res.json({
    status: true
  });
};

travelCtrl.updateTravel = async (req, res) => {
  const { id } = req.params
  Travel.findOne( { _id:id }, function(err, viaje)
  {
    if (err) return handleError(err)
    //if(err) return next(err)
    viaje.status = true
    viaje.weight = req.body.weight
    viaje.flete  = req.body.flete
    viaje.port = req.body.port
    viaje.bodega = req.body.bodega
    viaje.typevhicle = req.body.typevhicle
    viaje.cityOrigen = req.body.cityOrigen
    viaje.cityDestination = req.body.cityDestination
    viaje.save()
    res.json({ status:  true })
  })
  
};

travelCtrl.deleteTravel = async (req, res) => {
  Travel.findOne( { _id: req.params.id }, function(err, viaje)
  {
    if (err) return handleError(err)
    viaje.status = false
    viaje.save()
    res.json({ status:  true })
 })
};



module.exports = travelCtrl
