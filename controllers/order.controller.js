const ordertCtrl = {}
const Order =  require('../models/order')
const Product = require('../models/product')

ordertCtrl.getOrders = async (req, res ) => {
    const order = await Order.find()
    res.json(order)
}

ordertCtrl.getOrdersByUser = async (req, res ) => {
    const idUser = req.params.id
    const order = await Order.find({idUser:idUser , status:false})
    res.json({
        order
    })
    
}

ordertCtrl.getProductsOrder = async (req, res ) => {
    const id = req.params.id
    const full = []
    const { products } = await Order.findById(id)
    for (let i = 0; i < products.length; i++) {
        const element = products[i]._id;
        const product = await Product.findById(element).select('-img')
        full.push(product)
    }
    
    res.json(full)
    
    
}

ordertCtrl.getOrdersByBranch = async (req, res ) => {
    const id = req.params.id
    const order = await Order.find({idbranchoffice:id, status:true})
    res.json({
        order
    })
    
}


ordertCtrl.createOrder = async (req, res ) => {
    const products = req.body.products
    const invoice = req.body.invoice
    const branch = req.body.branchoffice
    const company = req.body.company
        const order = new Order({
            idInvoice : invoice,
            idUser : req.body.user,
            idbranchoffice: branch,
            products : products,
            price:  req.body.price,
            company:  company,
            status:  false,
            updated_at : new Date()
        })
        await order.save() 
        res.json({
            status: 'Orden registrada'
        })
        
}

ordertCtrl.updateOrder = async (req, res ) => {
    const id = req.params.id
    const order = {
        status : true
    }
    await Order.findByIdAndUpdate(id, {$set: order })
    res.json({
        status: 'Orden culminada'
    })
    
  }
   

module.exports = ordertCtrl