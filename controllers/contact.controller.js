const contactCtrl = {}

var mongoose = require("mongoose")
const Contact =  require('../models/contact')
mongoose.set('useFindAndModify', false)
const initSendEmail = require("../middlewares/mail")

contactCtrl.saveNewContact = async (req, res ) => {
    const contact = new Contact({
        name: req.body.name,
        email : req.body.email,
        phone: req.body.phone,
        msg: req.body.msg
    })
    initSendEmail.sendContact( req.body.email )
    await contact.save()
    res.json({ status: true })
}

module.exports = contactCtrl