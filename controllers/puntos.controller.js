const puntoCtrl = {};

const mongoose = require("mongoose");
const Punto = require("../models/puntos");

puntoCtrl.get = async (req, res) => {
  const punto = await Punto.find({ status: true })
  res.json(punto)
};

puntoCtrl.search = async (req, res) => {
  const punto = await Punto.findById(req.body._id)
  res.json(punto)
};

puntoCtrl.create = async (req, res) => {
  const punto = new Punto({
    name : req.body.name
  })
  await punto.save()
  res.json({
    status: true
  });
};

puntoCtrl.update = async (req, res) => {
  await Punto.findOne( { _id: req.body._id }, function(err, response){
    if (err) return handleError(err)
      response.name = req.body.name
      response.status = true
      response.save()
   })
   res.json({
    status: true
   });
};

puntoCtrl.delete = async (req, res) => {
  await Punto.findOne( { _id: req.body._id }, function(err, response){
    if (err) return handleError(err)
      response.status = false
      response.save()
   })
   res.json({
    status: true
   });
};


module.exports = puntoCtrl
