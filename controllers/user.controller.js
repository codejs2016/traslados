const userCtrl = {}

var mongoose = require("mongoose")
const User =  require('../models/user')
const bcrypt = require('bcrypt-nodejs')
mongoose.set('useFindAndModify', false)

userCtrl.getUsers = async (req, res ) => {
    const users = await User.find({})
    res.json(users)
}

userCtrl.getUserByName = async (req, res ) => {
    const user = await User.find({userName : req.params.userName})
    if (Object.keys(user).length != 0)
    {
        res.json({
            status:true,
            user
        })
    } 
    else
    {
        res.json({
            status:false
        })
    }   
}

userCtrl.createUser = async (req, res ) => {
    const user = new User({
        documentNumber: req.body.documentNumber,
        fullName: req.body.fullName,
        password: req.body.password,
        phone: req.body.phone,
        documentType: req.body.documentType,
        gender: req.body.gender,
        userName: req.body.userName,
        role: mongoose.Types.ObjectId(req.body.role)
    })
    await user.save()
    res.json({ status: true })
}

userCtrl.updateUser = async (req, res ) => {
    await User.findOne( { _id: req.body._id }, function(err, response){
        if (err) return handleError(err)
        response.documentNumber = req.body.documentNumber
        response.fullName = req.body.fullName
        response.password = req.body.password
        response.phone = req.body.phone
        response.documentType = req.body.documentType
        response.gender = req.body.gender
        response.userName = req.body.userName
        response.role = mongoose.Types.ObjectId(req.body.role)
        response.save()
    })
    res.json({
        status: true
    })        
}

userCtrl.deleteUser = async (req, res ) => {
    await User.findOne( { _id: req.body._id }, function(err, response){
        if (err) return handleError(err)
        response.status = false
        response.save()
    })
    res.json({
        status: true
    })        
}


userCtrl.updatePass = async(user) =>
{  
    for (let index = 0; index < user.length; index++) 
    {
        const id = user[index].id   
        var salt = bcrypt.genSaltSync(8)
        var hash = bcrypt.hashSync("123456", salt)
        const password = {
            password : hash
        } 
        await User.findByIdAndUpdate(id, {$set: password })
    }
}

module.exports = userCtrl