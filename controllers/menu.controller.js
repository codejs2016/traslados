const menuCtrl = {}
const Menu =  require('../models/menu')

menuCtrl.getMenuCompany = async (req, res ) => {
    const menu = await Menu.find({ type: 1 })
    res.json(menu)
}

menuCtrl.getMenuHome = async (req, res ) => {
    const menu = await Menu.find({ type: 2 })
    res.json(menu)
}

module.exports = menuCtrl