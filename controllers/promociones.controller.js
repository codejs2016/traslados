const promocionesCtrl = {}
const Promociones =  require('../models/promociones')

promocionesCtrl.getAll = async (req, res ) => {
   const promociones = await Promociones.find({ status: true })
   res.json(promociones)
}

promocionesCtrl.getAllSinImage = async (req, res ) => {
  const promociones = await Promociones.find({ status: true }).select("-imagen")
  res.json(promociones)
}

promocionesCtrl.savePromocion = async (req, res ) => {
  const newPromocio = new Promociones({
    name : req.body.name,
    imagen: req.body.imagen,
  })
  await newPromocio.save()
  res.json({ status : true })
}

promocionesCtrl.updatePromocion = async (req, res ) => {
  Promociones.findOne( { _id: req.body.id }, function(err, promocion){
    if (err) return handleError(err)
      promocion.name = req.body.name
      promocion.imagen = req.body.imagen
      user.save()
  })
}

promocionesCtrl.deletePromocion = async (req, res ) => {
  Promociones.findOne( { _id: req.params.id }, function(err, promocion){
    if (err) return handleError(err)
      promocion.status = false
      user.save()
  })
}

module.exports = promocionesCtrl