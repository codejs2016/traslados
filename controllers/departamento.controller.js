const departamentoCtrl = {}

const Departamento =  require('../models/departamento')

departamentoCtrl.getDepartamentos = async (req, res ) => {
    const dpts = await Departamento.find()
    res.json(dpts)
}

module.exports = departamentoCtrl