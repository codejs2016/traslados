const inspeccionCtrl = {}

const mongoose = require("mongoose")
const ctgInspeccion =  require('../models/ctginspeccion')

inspeccionCtrl.getAll = async (req, res ) => {
    const inspec = await ctgInspeccion.find({ status: true })
    res.json(inspec)
}
module.exports = inspeccionCtrl