const ctrlColorVehicle = {}

const colorVehicle = require("../models/colorVehicle");

ctrlColorVehicle.getAll = async (req, res) => {
  const colors = await colorVehicle.find({ status: true });
  res.json(colors);
};


module.exports = ctrlColorVehicle

