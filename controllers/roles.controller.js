const rolesCtrl = {};
const Roles = require("../models/role")

rolesCtrl.getRols = async (req, res) => {
  const roles = await Roles.find({ status: true })
  res.json(roles)
};

module.exports = rolesCtrl
