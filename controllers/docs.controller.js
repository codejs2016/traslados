const docCtrl = {}

const docAntecedente =  require('../models/doc')

docCtrl.getDocuments = async (req, res ) => {
    const docs = await docAntecedente.find({ user: req.params.user })
        .select('-created_at -user -__v -documento')
        .populate('type')
    res.json(docs)
}

docCtrl.removeDocument = async (req, res) => {
    await docAntecedente.findByIdAndRemove(req.params.id)
    res.json({ status: true })
}

module.exports = docCtrl