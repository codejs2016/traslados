const driverCtrl = {}
const Driver =  require('../models/driver')

driverCtrl.getConductors = async (req, res ) => {
   const driver = await Driver.find({ status: true })
   res.json(driver)
}


driverCtrl.getCvConductorById = async (req, res ) => {
   console.log( req.params.id )
   const driver = await Driver.find({ _id: req.params.id })
               .populate('cvvehicle')
               //.populate('refcompany')
               //.populate('reffamiliar')
               //.populate('docs')
   res.json( driver )
}

driverCtrl.search = async (req, res ) => {
   const foundDriver = await Driver.findOne({ documentNumber: req.body.documentdriver })
   .select("-created_at -__v -password")
   if(foundDriver)
     res.json(foundDriver)
   else
     res.json({ status : false })
}


driverCtrl.update = async (req, res ) => {
   Driver.findOne( { _id: req.body.id }, function(err, driver){
      if (err) return handleError(err)
      driver.city = req.body.city
      driver.save()
      res.json({ status: true })
   })
}
module.exports = driverCtrl