const categorytCtrl = {}
const Category =  require('../models/category')

categorytCtrl.getCategorys = async (req, res ) => {
    const category = await Category.find()
    res.json(category)
}

categorytCtrl.getCategory = async (req, res ) => {
    const category = await Category.findById(req.params.id)
    res.json(category)
}

categorytCtrl.getCategoryType = async (req, res ) => {
    const category = await Category.find({type:req.params.type})
    res.json(category)
}

categorytCtrl.searchCategory = async (req, res ) => {
    const category = await Category.find({'name' : req.body.name })
    if( category )
    {
        res.status(200).send({ category , status : true })
    }else
    {
        res.json({
            status: false
        })
    }
}

categorytCtrl.createCategory = async (req, res ) => {
    const category = new Category({
        name : req.body.name
    })
    await category.save()
    res.json({
        status: true
    })
}

categorytCtrl.deleteCategory = async (req, res) => {
    await Category.findOne( { _id: req.body._id }, function(err, response){
        if (err) return handleError(err)
          response.status = false
          response.save()
       })
       res.json({
           status: true
       })
};

categorytCtrl.updateCategory = async (req, res ) => {
   await Category.findOne( { _id: req.body._id }, function(err, response){
    if (err) return handleError(err)
      response.name = req.body.name
      response.status = true
      response.save()
   })
   res.json({
       status: true
   })
}

module.exports = categorytCtrl