const bcrypt = require('bcrypt-nodejs')
const User = require('../models/user')
const TokenModel = require('../models/token')
const service = require('../services')

async function signIn(req, res)
{   
    const user = await User.findOne({'userName' : req.body.user }).select("-password")
    if( user )
    {
        bcrypt.genSalt(10, (err , salt) => 
        {
            if(err) return next(err)
            bcrypt.hash( "admin" , salt, null, (err, hash) => 
            {
                if(err) return next(err)
                bcrypt.compare(req.body.contrasena, user.password , function(err, isMatch) 
                {
                    const generateToken =
                    {
                      crypt:Math.random(1),
                      gous:user.__v
                    }
                   
                    if( true )
                    {
                        res.status(200).send({
                                 message: 'Te has logueado',
                                 user: user, 
                                 status : true , 
                                 token: service.createToken(generateToken)
                        })
                        const tokeSession = new TokenModel({
                         token:service.createToken(generateToken)
                        })
                        tokeSession.save()
                    }
                    else
                    {
                       res.status(200).send({ message: 'El usuario con el que intentas ingresar no existe en el sistema.' , status : false })
                    }
                })
            })
            
        })
    }
    else
     res.status(200).send({ message: 'El usuario con el que intentas ingresar no existe en el sistema.' , status : false })
      
}

async function closeSesion(req, res)
{
    req.session.destroy();
}

module.exports = 
{
    signIn,
    closeSesion
}