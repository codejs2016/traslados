const productCtrl = {};

const mongoose = require("mongoose");
const Product = require("../models/product");
const TallasProducto = require("../models/tallas");

const xlsxj = require("xlsx-to-json");
const fs = require('fs');

productCtrl.get = async (req, res) => {
  const product = await Product.find({ status: true }).select("-created_at");
  res.json(product);
};

productCtrl.search = async (req, res) => {
  const product = await Product.find({
    name: { $regex: ".*" + req.body.name.toLowerCase() + ".*" },
    status: true
  });
  if (product) {
    res.status(200).send({ product, status: true });
  } else {
    res.json({
      status: false
    });
  }
};

productCtrl.create = async (req, res) => {
  const product = new Product({
    name: req.body.name,
    ctg: req.body.ctg,
    vunit: req.body.vunit,
    vventa: req.body.vventa,
    cnt: req.body.cnt,
    stock: req.body.cnt,
    tipoEmpaque: req.body.tipoEmpaque,
    cntUnd: req.body.cntUnd,
    ref: req.body.ref,
    vtotal: req.body.cnt * req.body.vunit,
    proveedor:  req.body.proveedor,
    talla:  req.body.talla,
    color:  req.body.color
  });
  await product.save();
  res.json({
    status: true
  });
};


productCtrl.createTallas = async (req, res) => {
  for(let i = 0; i < req.body.length; ++i) {
   const product = new TallasProducto({
    producto: req.body[i].name,
    talla: req.body[i].talla,
    cnt: req.body[i].cnt,
   })
   await product.save();
  }
  res.json({
    status: true
  });
};


productCtrl.getTallas = async (req, res) => {
  const product = await TallasProducto.find({ producto: req.body.name });
  res.status(200).send({ product, status: true });
};

productCtrl.deleteTallas = async (req, res) => {
  await TallasProducto.findByIdAndRemove(req.body.id);
  res.json({
    status: true
  });
};


productCtrl.getAllTallasProducto = async (req, res) => {
  const product = await TallasProducto.find();
  res.status(200).send({ product, status: true });
};


productCtrl.update = async (req, res) => {
  await Product.findOne( { _id: req.body._id }, function(err, response){
    if (err) return handleError(err)
      response.name = req.body.name
      response.ctg = req.body.ctg
      response.vunit = req.body.vunit
      response.vventa = req.body.vventa
      response.cnt = req.body.cnt
      response.tipoEmpaque = req.body.tipoEmpaque
      response.status = true
      response.cntUnd = req.body.cntUnd,
      response.ref = req.body.ref
      response.save()
   })
   res.json({
    status: true
   });
};

productCtrl.delete = async (req, res) => {
  await Product.findOne( { _id: req.body._id }, function(err, response){
    if (err) return handleError(err)
      response.status = false
      response.save()
   })
   res.json({
    status: true
   });
};

productCtrl.upload = async (req, res) => {
  console.log( req.files );
  fs.readFile(req.files.file.path, function(err, data){
    // Do something with the data (which holds the file information)
    console.log(data);
  });
  /*xlsxj({
    input: "Libro.xlsx", 
    output: "output.json"
  }, function(err, result) {
    if(err) {
      console.error(err);
    }else {
      console.log(result);
    }
  });*/
   res.json({
    status: true
   });
};

module.exports = productCtrl;
