const salesCtrl = {}

const mongoose = require("mongoose")

const Sales =  require('../models/sale')
const Company =  require('../models/company')

salesCtrl.getSales = async (req, res ) => {
    const sales = await Sales.find({status: true})
    res.json(sales)
}

salesCtrl.getSalebycompany = async (req, res ) => {
    const sales = await Sales.find({idcompany:req.params.id, status:true})
    res.json({
        status:true,
        sales
    })
}


salesCtrl.createSale = async (req, res ) => {
    const products = req.body.products
    const company = req.body.company
        const sale = new Sales({
            name : req.body.name,
            description : req.body.description,
            img: req.body.img,
            products : products,
            price:  req.body.price,
            idcompany:  company,
            status:  true,
            updated_at : new Date()
        })
        await sale.save() 
        res.json({
            status: 'Promoción registrada'
        })
}



module.exports = salesCtrl