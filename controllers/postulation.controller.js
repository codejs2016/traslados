 const postulationCtrl = {}
 const Postulation =  require('../models/postulation')

 postulationCtrl.getPostulations = async (req, res ) => {
    const postulations = await Postulation.find()
           .populate('conductor travel')
    res.json(postulations)
 }

 postulationCtrl.savePostulation = async (req, res ) => {
     const postulations = new Postulation({
         conductor : mongoose.Types.ObjectId(req.body.userconductor),
         travel :  mongoose.Types.ObjectId(req.body.travelid)
     })
     const newPostulation = await postulations.save()
     res.json(newPostulation)
 }

 module.exports = postulationCtrl