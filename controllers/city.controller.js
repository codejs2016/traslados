const cityCtrl = {}

const mongoose = require("mongoose")
const City =  require('../models/city')
const CitysAndDepartment =  require('../models/departmentsCitys')

const Company = require('../models/company')
const Tipocarga = require('../models/tipocarga')
const Puerto = require('../models/puertos')
const TipoVehiculo = require('../models/vehicles')
const Travel = require('../models/travel')

cityCtrl.getTipoVehiculo = async (req, res ) => {
    const tipoVehiculo = await TipoVehiculo.find( { status: true } )
    res.json(tipoVehiculo)
}

cityCtrl.getCarga = async (req, res ) => {
    const carga = await Tipocarga.find( { status: true } )
    res.json(carga)
}

cityCtrl.getPuerto = async (req, res ) => {
    const puerto = await Puerto.find( { status: true } )
    res.json(puerto)
}

cityCtrl.getCitys = async (req, res ) => {
    const city = await CitysAndDepartment.find().select("ciudades")
    res.json(city)
}

cityCtrl.getCitysAndCompany = async (req, res ) =>{
   //const  params = req.body
   Company.find( { status: true, city:element.id } )
       .populate('city')
       .exec(function (err, results) {
           if (err) return handleError(err);
           res.json( results )
   })
   
    /*params.forEach(element => {
     // const companys = Company.find( { status: true, city:element.id } ).select("-img -contacto -password -nit -role -status -city -direccion -__v")
      Company.find( { status: true, city:element.id } )
       .populate('city')
       .exec(function (err, results) {
           if (err) return handleError(err);
           res.json( results )
      })
   })*/
}

    /*const city = await City.find( { status: true } )
    const citys = []
    const getTravels = []
    for (let i = 0; i < city.length; i++){
        
        const c = city[i];
        citys.push({
            name: c.name,
        })

      const companys = await Company.find( { status: true, city:c._id } ).select("-img -contacto -password -nit -role -status -city -direccion -__v")
        for (let j = 0; j < companys.length; j++) {
            const rs  = await Travel.find({ company : companys[j]._id }).select("-_id -status -weight -flete -created_at -cityOrigen -cityDestination -port -typevhicle -updated_at -__v")
            citys.push(rs)
        }
    }
    res.json({
        status: true,
        citys
    });*/


cityCtrl.getCity = async (req, res ) => {
    const city = await City.findById(req.params.id)
    res.json(city)
}

cityCtrl.searchCity = async (req, res ) => {
   const city = await City.find({'name': { $regex: '.*' + req.body.name + '.*' } })
   res.json( city )
}

cityCtrl.createCity = async (req, res ) => {
    const city = new City({
        name : req.body.name
     })
    await city.save()
     res.json({
         status: 'Ciudad registrada'
    })
}

cityCtrl.updateCity = async (req, res ) => {
    const city = new City({
        name : req.body.name,
        _id: req.body.id
     })
    await City.findByIdAndUpdate(req.body.id, {$set: city }, {new :true })
    res.json({
         status: 'Ciudad actualizada'
    })
 }

 cityCtrl.deleteCity = async (req, res) => {
     console.log(  req.params )
     
   // await City.findByIdAndDelete( req.params.id )
    res.json({
        status: 'Ciudad eliminada'
    })
}

module.exports = cityCtrl