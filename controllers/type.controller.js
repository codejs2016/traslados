const typeCtrl = {}
const TypePhoto =  require('../models/typephoto')

typeCtrl.getTypePhotos = async (req, res ) => {
   const typePhotos = await TypePhoto.find()
   res.json(typePhotos)
}

module.exports = typeCtrl