const photoCtrl = {}
const Photo =  require('../models/photo')

photoCtrl.getPhotosAll = async (req, res ) => {
   const photo = await Photo.find()
   res.json(photo)
}

photoCtrl.getPhotos = async (req, res ) => {
   const photo = await Photo.find({ plate: req.params.id })
   if(photo)
     res.json(photo)
   else 
     res.json({ status: false })
}


photoCtrl.deletePhoto = async (req, res ) => {
  const photo = await Photo.findByIdAndRemove(req.params.id)
  if(photo)
    res.json({status:true})
  else 
    res.json({ status: false })
}


module.exports = photoCtrl