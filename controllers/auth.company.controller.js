const bcrypt = require('bcrypt-nodejs')
const TokenModel = require('../models/token')
const service = require('../services')

async function signIn(req, res)
{   
    const user = await TokenModel.findOne({'token' : req.body.token })
    if( user )
    {
        res.status(200).send({ user })
    }
    else
    {
        res.status(200).send({ message: 'El usuario con el que intentas ingresar no existe en el sistema.' , status : false })
    }
}

async function closeSesion(req, res)
{
    req.session.destroy();
}

  
module.exports = 
{
    signIn,
    closeSesion
}