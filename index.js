const express = require("express");
const morgan = require("morgan");
const cors = require("cors");
const moment = require('moment')
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const session = require("express-session");
const redis = require("redis")
const cluster = require("cluster");
const config = require("./config");
const { mongoose } = require("./database");
const bcrypt = require('bcrypt-nodejs')
const mongooseLib = require("mongoose")
const Company =  require('./models/company')
const TokenModel = require('./models/token')
const User =  require('./models/user')
const Photo =  require('./models/photo')
const Driver =  require('./models/driver')
const CvVehicle = require('./models/cv_vehicle')
const Promocion =  require('./models/promociones')
const DocAntecedente =  require('./models/doc')
const initSendEmail = require("./middlewares/mail");
const initData = require("./models/initData")
const app = express();



/*
initData.createUser()dd
initData.createDocumentType()
initData.createCategory()
initData.createGender()
initData.createRol()*/

const http = require("http").Server(app);
const io = require("socket.io")(http);

io.on("connection", function(socket) {
  console.log("usuario conectado " + socket.id);
  socket.on("disconnect", function() {
    console.log("usuario desconectado " + socket.id);
    // io.sockets.emit('pedido', msg)
  });
 
  socket.on("traslado", data => {
    console.log( data );
   io.sockets.emit('traslado', data)
  })
 
})



require("./system/prototype");
global.config = require("./in18");
global.i18n = require("./system/helpers/i18n");

global.i18n.setLanguage();

//Settings
app.set("port", process.env.PORT || 3000);
app.use(
  session({
    secret: "AsdalkAasdkaSDSS*SASDAS@",
    saveUninitialized: true,
    resave: true,
    cookie: { maxAge: 8 * 60 * 60 * 1000 }
  })
);

//app.use(logger("dev"));
app.use(express.json({ limit: "50mb" }));
//balanceo de carga con 6mb
app.use(express.urlencoded({ limit: "50mb", extended: true }));
app.use(cookieParser());

//Middlewares 
app.use(morgan("dev"));
app.use(cors())


app.all('/*', function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers',
             'Content-Type,X-Requested-With,cache-control,pragma');
  next();
});


//initData.createDeptCity()
//initData.createPostulaciones()
//initData.createMenu()
//initData.createTypeDocumentsDriver()
//initData.createColorVehicle()
//initData.createRol()
//initData.createPromociones()
//initData.createDocumentType()
//initData.createGender()
//initData.createUser()
//initData.createCity()
//initData.createCompany()
//initData.createCategory()
//initData.createProduct()
//initData.createPuerto()
//initData.createTipoCarga()
//initData.createVehiculos()
//initData.createCtgInpeccion()

app.use("/api/auth", require("./routes/auth.routes"))
app.use("/api/puntos", require("./routes/puntos.routes"))
app.use("/api/category", require("./routes/category.routes"))
app.use("/api/product", require("./routes/product.routes"))
app.use("/api/user", require("./routes/user.routes"))
app.use("/api/traslado", require("./routes/traslado.routes"))
app.use("/api/documentypes", require("./routes/documentType.routes"))
app.use("/api/token", require("./routes/tokenvalidate.routes"))
//app.use("/api/confirm", require("./routes/confirmation.routes"))
//app.use("/api/company", require("./routes/company.routes"))
//app.use("/api/city", require("./routes/city.routes"))
app.use("/api/gender", require("./routes/routes.gender"))
//app.use("/api/resetpass", require("./routes/resetpass.routes"))
//app.use("/api/branchoffice", require("./routes/branchoffice.routes"))
//app.use("/api/invoice", require("./routes/invoice.routes"))
//app.use("/api/order", require("./routes/order.routes"))
//app.use("/api/factureref", require("./routes/factureref.routes"))
//app.use("/api/charts", require("./routes/charts.routes"))
//app.use("/api/sales", require("./routes/sales.routes"))
//app.use("/api/hours", require("./routes/hour.routes"))
//app.use("/api/adds", require("./routes/add.routes"))
//app.use("/api/travel", require("./routes/travel.routes"))
app.use("/api/rols", require("./routes/role.routes"))
//app.use("/api/refempresarial", require("./routes/refempresarial.routes"))
//app.use("/api/vehicle", require("./routes/vehicle.routes"))
//app.use("/api/menu", require("./routes/menu.routes"))
//app.use("/api/contact", require("./routes/contact.routes"))
//app.use("/api/photo", require("./routes/photos.route"))
//app.use("/api/type", require("./routes/type.routes"))
//app.use("/api/promocion", require("./routes/promocion.routes"))
//app.use("/api/docs", require("./routes/docs.routes"))
//app.use("/api/driver", require("./routes/driver.routes"))
//app.use("/api/postulation", require("./routes/postulation.routes"))
//app.use("/api/sedes", require("./routes/sedes.routes"))
//app.use("/api/deparmentscity", require("./routes/departmentcity.routes"))
//app.use("/api/ubicacion", require("./routes/ubicacion.routes"))
//app.use("/api/validate", require("./routes/utils.routes"))
//app.use("/api/color", require("./routes/colorvehicle.routes"))
//app.use("/api/inspeccion", require("./routes/inspeccion.routes"))
//app.use("/api/storage", require("./routes/storage.routes"))
//app.use("/api/background", require("./routes/backgroundLocation.routes"))


const numCPUs = require('os').cpus().length;
/*if (cluster.isMaster)
{
  masterProcess();
}
else
{
  childProcess();
}*/



function masterProcess() {
  console.log(`Master ${process.pid} is running`);

  for (let i = 0; i < numCPUs; i++) {
    console.log(`Forking process number ${i}...`);

    cluster.fork();
  }

  cluster.on("exit", (worker, code, signal) => {
    console.log(`Worker ${worker.process.pid} died`);
    console.log(`...Forking a new process...`);

    cluster.fork();
  });
}

function childProcess() {
  console.log(`Worker ${process.pid} started...`);
  http.listen(config.port, function() {
    console.log(`Server Runing on PORT ${config.port}`);
  });
}

// //Starting the server
http.listen(app.get("port"), () => {
  console.log("Server on port ", app.get("port"));
});
