const express = require('express')
const router =  express.Router()

const confirmationCtrl = require('../controllers/confirmation.controller')

router.get('/:id', confirmationCtrl.enableAccountCompany)


module.exports = router