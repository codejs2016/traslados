const express = require('express')
const router =  express.Router()

const photosCtrl = require('../controllers/photos.controller')

router.get('/', photosCtrl.getPhotosAll)
router.get('/:id', photosCtrl.getPhotos)
router.delete('/:id', photosCtrl.deletePhoto)
module.exports = router