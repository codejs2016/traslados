const express = require('express')
const router =  express.Router()

const menuCtrl = require('../controllers/menu.controller')

router.get('/', menuCtrl.getMenuCompany)
router.get('/home', menuCtrl.getMenuHome)
module.exports = router