const express = require('express')
const router =  express.Router()

const userCtrl = require('../controllers/user.controller')

router.get('/', userCtrl.getUsers)
router.post('/create', userCtrl.createUser)
router.post('/update', userCtrl.updateUser)
router.post('/delete', userCtrl.deleteUser)

module.exports = router