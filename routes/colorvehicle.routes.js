const express = require('express')
const router =  express.Router()

const colorCtrl = require('../controllers/colorVehicle.controller')

router.get('/', colorCtrl.getAll)

module.exports = router