const express = require('express')
const router =  express.Router()

const deptCtrl = require('../controllers/departamento.controller')

router.get('/', deptCtrl.getDepartamentos)

module.exports = router