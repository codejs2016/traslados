const express = require('express')
const router =  express.Router()

const authCtrl = require('../controllers/auth.company.controller')

router.post('/', authCtrl.signIn)

module.exports = router