const express = require('express')
const router =  express.Router()

const typeCtrl = require('../controllers/type.controller')

router.get('/', typeCtrl.getTypePhotos)


module.exports = router