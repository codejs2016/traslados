const express = require('express')
const router =  express.Router()

const contactCtrl = require('../controllers/contact.controller')

router.post('/', contactCtrl.saveNewContact)

module.exports = router