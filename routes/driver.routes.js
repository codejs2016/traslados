const express = require('express')
const router =  express.Router()

const driverCtrl = require('../controllers/driver.controller')

router.get('/', driverCtrl.getConductors)
router.get('/:id', driverCtrl.getCvConductorById)
router.post('/search', driverCtrl.search)
router.post('/update', driverCtrl.update)

module.exports = router