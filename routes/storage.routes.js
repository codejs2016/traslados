const express = require('express')
const router =  express.Router()

const storageCtrl = require('../controllers/storage.controller')

router.post('/', storageCtrl.save)
router.get('/', storageCtrl.getAll)
router.get('/:id', storageCtrl.getById)

module.exports = router