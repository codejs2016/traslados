const express = require('express')
const router =  express.Router()

const utilsCtrl = require('../controllers/utils.controller')

router.post('/', utilsCtrl.existPlate)

module.exports = router