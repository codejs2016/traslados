const express = require('express')
const router =  express.Router()

const productCtrl = require('../controllers/product.controller')

router.get('/', productCtrl.get)
router.post('/search', productCtrl.search)
router.post('/create', productCtrl.create)
router.post('/tallas', productCtrl.createTallas)
router.post('/getTallas', productCtrl.getTallas)
router.post('/deletetalla', productCtrl.deleteTallas)
router.get('/getAllTallasProducto', productCtrl.getAllTallasProducto)
router.post('/update', productCtrl.update)
router.post('/delete', productCtrl.delete)
router.post('/upload', productCtrl.upload)
module.exports = router