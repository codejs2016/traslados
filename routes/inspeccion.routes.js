const express = require('express')
const router =  express.Router()

const ctgInspeccionCtrl = require('../controllers/inspeccion.controller')
router.get('/', ctgInspeccionCtrl.getAll)

module.exports = router