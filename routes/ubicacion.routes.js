const express = require('express')
const router =  express.Router()

const ubicacionCtrl = require('../controllers/ubicacion.controller')
router.post('/', ubicacionCtrl.create)

module.exports = router