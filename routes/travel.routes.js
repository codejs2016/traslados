const express = require('express')
const router =  express.Router()

const travelCtrl = require('../controllers/travel.controller')

router.get('/', travelCtrl.getTravels)
router.get('/:id', travelCtrl.getTravelsByCompany)
router.post('/create', travelCtrl.createTravel)
router.post('/search', travelCtrl.search)
router.put('/:id', travelCtrl.updateTravel)
router.delete('/:id', travelCtrl.deleteTravel)

module.exports = router