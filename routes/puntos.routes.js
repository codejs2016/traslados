const express = require('express')
const router =  express.Router()

const puntoCtrl = require('../controllers/puntos.controller')

router.get('/', puntoCtrl.get)
router.post('/', puntoCtrl.create)
router.post('/update', puntoCtrl.update)
router.post('/delete', puntoCtrl.delete)
module.exports = router