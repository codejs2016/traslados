const express = require('express')
const router =  express.Router()

const salesCtrl = require('../controllers/sales.controller')

router.get('/', salesCtrl.getSales)
router.get('/company/:id', salesCtrl.getSalebycompany)
router.post('/', salesCtrl.createSale)

module.exports = router