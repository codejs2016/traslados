const express = require('express')
const router =  express.Router()

const postulationCtrl = require('../controllers/postulation.controller')

router.get('/', postulationCtrl.getPostulations)
router.post('/', postulationCtrl.savePostulation)



module.exports = router