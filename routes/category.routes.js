const express = require('express')
const router =  express.Router()

const categoryCtrl = require('../controllers/category.controller')

router.get('/', categoryCtrl.getCategorys)
router.get('/:id', categoryCtrl.getCategory)
router.post('/create', categoryCtrl.createCategory)
router.post('/search', categoryCtrl.searchCategory)
router.post('/update', categoryCtrl.updateCategory)
router.post('/delete', categoryCtrl.deleteCategory)

module.exports = router