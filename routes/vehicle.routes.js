const express = require('express')
const router =  express.Router()

const vehicleCtrl = require('../controllers/cv.vehicles.controller')

router.get('/plates', vehicleCtrl.getVehiclesPlacas)
router.get('/', vehicleCtrl.getVehicles)
router.get('/:id', vehicleCtrl.getVehicle)
router.get('/driver/:id', vehicleCtrl.getVehiclePlatesAndDriver)
router.post('/search', vehicleCtrl.searchVehicle)
router.post('/create', vehicleCtrl.createVehicleWithPropietary)
router.post('/createweb', vehicleCtrl.createVehicleWithPropietaryWeb)
router.put('/:id', vehicleCtrl.updateVehicleWithPropietary)
router.delete('/:id', vehicleCtrl.deleteVehicleWithPropietary)

module.exports = router