const express = require('express')
const router =  express.Router()

const refempresarialCtrl = require('../controllers/reff.company.controller')

router.get('/driver/:id', refempresarialCtrl.getByDriver)
router.post('/create', refempresarialCtrl.create)
module.exports = router