const express = require('express')
const router =  express.Router()

const companyCtrl = require('../controllers/company.controller')

router.get('/', companyCtrl.getCompanys)
router.get('/:id', companyCtrl.getCompany)
router.post('/create', companyCtrl.createCompany)
router.post('/citycompanys', companyCtrl.getCompanyByCity)
router.post('/search', companyCtrl.searchCompany)
router.put('/:id', companyCtrl.updateCompany)
router.delete('/', companyCtrl.deleteCompany)

module.exports = router