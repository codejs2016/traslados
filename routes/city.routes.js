const express = require('express')
const router =  express.Router()

const cityCtrl = require('../controllers/city.controller')

router.get('/', cityCtrl.getCitys)
router.post('/citycompanys', cityCtrl.getCitysAndCompany)
router.get('/carga', cityCtrl.getCarga)
router.get('/puertos', cityCtrl.getPuerto)
router.get('/vehicles', cityCtrl.getTipoVehiculo)
router.get('/:id', cityCtrl.getCity)
router.post('/create', cityCtrl.createCity)
router.post('/search', cityCtrl.searchCity)
router.put('/:id', cityCtrl.updateCity)
router.delete('/', cityCtrl.deleteCity)

module.exports = router