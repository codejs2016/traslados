const express = require('express')
const router =  express.Router()

const auth = require('../middlewares/auth')
const authCtrl = require('../controllers/auth.controller')
global.config = require('../in18')

router.post('/', authCtrl.signIn)
router.get('/islog/:token', auth.isLogged)

module.exports = router