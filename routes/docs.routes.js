const express = require('express')
const router =  express.Router()

const docsCtrl = require('../controllers/docs.controller')

router.get('/:user', docsCtrl.getDocuments)
router.delete('/:id', docsCtrl.removeDocument)

module.exports = router