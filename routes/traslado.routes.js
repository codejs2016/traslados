const express = require('express')
const router =  express.Router()

const trasladoCtrl = require('../controllers/traslado.controller')

router.get('/', trasladoCtrl.get)
router.post('/create', trasladoCtrl.create)
router.post('/search', trasladoCtrl.search)
router.post('/searchTraslado', trasladoCtrl.searchTraslado)
router.post('/searchTrasladoByFecha', trasladoCtrl.searchTrasladoByFecha)
router.post('/get', trasladoCtrl.getByPunto)
router.get('/stock/:id', trasladoCtrl.validateStock)
module.exports = router