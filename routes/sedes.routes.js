const express = require('express')
const router =  express.Router()

const sedesCtrl = require('../controllers/sedes.controller')

router.post('/', sedesCtrl.createSede)
router.post('/get', sedesCtrl.getSedes)
router.post('/save', sedesCtrl.saveSedes)

module.exports = router