const express = require('express')
const router =  express.Router()

const roleCtrl = require('../controllers/roles.controller')

router.get('/', roleCtrl.getRols)
module.exports = router