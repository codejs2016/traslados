const express = require('express')
const router =  express.Router()

const deparmentCityCtrl = require('../controllers/departmentcity.controller')

router.get('/', deparmentCityCtrl.getAll)

module.exports = router