const express = require('express')
const router =  express.Router()

const promocionCtrl = require('../controllers/promociones.controller')

router.get('/', promocionCtrl.getAll)
router.get('/sin', promocionCtrl.getAllSinImage)
router.post('/', promocionCtrl.savePromocion)
router.put('/:id', promocionCtrl.updatePromocion)
router.delete('/:id', promocionCtrl.deletePromocion)

module.exports = router