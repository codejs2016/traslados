"use strict";
const nodemailer = require("nodemailer")
const path = require('path')
const smtpTransport = require('nodemailer-smtp-transport');
const TokenModel = require('../models/token')
const services = require('../services')
const TypePhotos = require('../models/typephoto')

/*const transporter = nodemailer.createTransport({
  //host: "smtp.gmail.com",
  //port: 587,
  //secure: false,
  service: 'gmail',
  auth: {
    user: "codebydevs@gmail.com",
    pass: "JavierX88*"
  }
});*/

const transporter = nodemailer.createTransport(smtpTransport({
  host: 'mail.sigma7.com.co',
  port: 587,
  tls:{
    rejectUnauthorized: false
  },
  auth: {
    user: 'jhon.renteria@sigma7.com.co',
    pass: 'jjrh12345678'
  }
}));

exports.sendSubscription =  function(emailsuscription, token)
{
  var newToken = services.createToken({ token:token })
  const tokeSession = new TokenModel({
    token: newToken,
    company: token
  })
  tokeSession.save()

 var htmlCustom = '<div style="width:auto;height:auto; background:#4466f4; color:white; padding:30px; margin:15px 0;text-align: center;-webkit-transition: background-image 0.3s ease-out;-moz-transition: background-image 0.3s ease-out;-o-transition: background-image 0.3s ease-out;transition: background-image 0.3s ease-out;box-shadow: 0px 0px 43px 0px rgba(131, 131, 131, 0.23);"><img src="https://truckerweb.herokuapp.com/assets/img/Logo_Trucker_Blanco_web.png"><p>Bienvenido a la comunidad TRUCKER, Recuerda que estamos trabajando para ti!</p><a style=" text-decoration: none;font-size: 14px;padding: 5px 14px;border-radius: 30px;font-family:sans-serif;letter-spacing: 1px;font-weight: 600;color: #fff;border: none;text-transform: uppercase;-webkit-transition: all 0.3s ease-in-out;-moz-transition: all 0.3s ease-in-out;-o-transition: all 0.3s ease-in-out;transition: all 0.3s eas; background: #fff; border-color: #fff; color: #3c9cfd;position: relative;z-index: 1;" href="http://localhost:4203/confirm/'+newToken+'" >ACTIVAR CUENTA</a></div><div style="margin-bottom:20px;width:100%;vertical-align: bottom;padding-bottom: 30px;background-color: #f7f7f7;"><img src="https://truckerweb.herokuapp.com/assets/img/Logo_Trucker_Blanco_web.png" style="width:57px;"><br>Cra 6 # 56 Calle Primera · Buenventura Valle<br> Centro de ayuda · Política de privacidad · Términos y condiciones<br> Dejar de recibir mensajes de este correo en '+emailsuscription+' </div></div>' 
 var mailOptions = {
  from: 'codebydevs@gmail.com',
  to: emailsuscription,
  subject: 'Notificación de creación de empresa ',
  html: htmlCustom
};

transporter.sendMail(mailOptions, function(error, info){
  if (error) {
    console.log(error);
  } else {
    console.log('Email sent: ' + info.response);
  }
})  

}

exports.sendEnableAccountCompany =  function(emailsuscription)
{
 var htmlCustom = '<div style="width:auto;height:auto; background:#4466f4; color:white; padding:30px; margin:15px 0;text-align: center;-webkit-transition: background-image 0.3s ease-out;-moz-transition: background-image 0.3s ease-out;-o-transition: background-image 0.3s ease-out;transition: background-image 0.3s ease-out;box-shadow: 0px 0px 43px 0px rgba(131, 131, 131, 0.23);"><img src="https://truckerweb.herokuapp.com/assets/img/Logo_Trucker_Blanco_web.png"><p>Bienvenido a la comunidad TRUCKER, Recuerda que estamos trabajando para ti!</p><a style=" text-decoration: none;font-size: 14px;padding: 5px 14px;border-radius: 30px;font-family:sans-serif;letter-spacing: 1px;font-weight: 600;color: #fff;border: none;text-transform: uppercase;-webkit-transition: all 0.3s ease-in-out;-moz-transition: all 0.3s ease-in-out;-o-transition: all 0.3s ease-in-out;transition: all 0.3s eas; background: #fff; border-color: #fff; color: #3c9cfd;position: relative;z-index: 1;" href="http://localhost:4201/login">TÚ CUENTA EMPRESARIAL HA SIDO ACTIVADA, INGRESA !</a></div><div style="margin-bottom:20px;width:100%;vertical-align: bottom;padding-bottom: 30px;background-color: #f7f7f7;"><img src="https://truckerweb.herokuapp.com/assets/img/Logo_Trucker_Blanco_web.png" style="width:57px;"><br>Cra 6 # 56 Calle Primera · Buenventura Valle<br> Centro de ayuda · Política de privacidad · Términos y condiciones<br> Dejar de recibir mensajes de este correo en '+emailsuscription+' </div></div>' 
 var mailOptions = {
  from: 'codebydevs@gmail.com',
  to: emailsuscription,
  subject: 'Notificación de activación cuenta empresarial ',
  html: htmlCustom
};

transporter.sendMail(mailOptions, function(error, info){
  if (error) {
    console.log(error);
  } else {
    console.log('Email sent: ' + info.response);
  }
})  

}

exports.sendContact =  function(email)
{
  
 var htmlCustom = '<div style="width:auto;height:auto; background:#4466f4; color:white; padding:30px; margin:15px 0;text-align: center;-webkit-transition: background-image 0.3s ease-out;-moz-transition: background-image 0.3s ease-out;-o-transition: background-image 0.3s ease-out;transition: background-image 0.3s ease-out;box-shadow: 0px 0px 43px 0px rgba(131, 131, 131, 0.23);"><img src="https://truckerweb.herokuapp.com/assets/img/Logo_Trucker_Blanco_web.png"><p>Bienvenido a la comunidad TRUCKER, Recuerda que estamos trabajando para ti!</p><a style=" text-decoration: none;font-size: 14px;padding: 5px 14px;border-radius: 30px;font-family:sans-serif;letter-spacing: 1px;font-weight: 600;color: #fff;border: none;text-transform: uppercase;-webkit-transition: all 0.3s ease-in-out;-moz-transition: all 0.3s ease-in-out;-o-transition: all 0.3s ease-in-out;transition: all 0.3s eas; background: #fff; border-color: #fff; color: #3c9cfd;position: relative;z-index: 1;" >NOS COMUNICAREMOS CONTIGO</a></div><div style="margin-bottom:20px;width:100%;vertical-align: bottom;padding-bottom: 30px;background-color: #f7f7f7;"><img src="https://truckerweb.herokuapp.com/assets/img/Logo_Trucker_Blanco_web.png" style="width:57px;"><br>Cra 6 # 56 Calle Primera · Buenventura Valle<br> Centro de ayuda · Política de privacidad · Términos y condiciones<br> Dejar de recibir mensajes de este correo en '+email+' </div></div>'
 var mailOptions = {
  from: 'codebydevs@gmail.com',
  to: email,
  subject: 'Gracias por ponerte en contacto con TRUCKER ',
  html: htmlCustom,
 };

 transporter.sendMail(mailOptions, function(error, info){
  if (error) {
    console.log(error);
  } else {
    console.log('Email sent: ' + info.response);
  }
 })  
}


exports.notificationDocsDriver =   async  function(email)
{
 var names = []
 const typePhoytosDocuments = await TypePhotos.find({ type : 2 })
  for (let i = 0; i < typePhoytosDocuments.length; i++) {
     names.push( typePhoytosDocuments[i].name+"<br>")
  }

  const htmlCustom = '<div style="width:auto;height:auto; background:#4466f4; color:white; padding:30px; margin:15px 0;text-align: center;-webkit-transition: background-image 0.3s ease-out;-moz-transition: background-image 0.3s ease-out;-o-transition: background-image 0.3s ease-out;transition: background-image 0.3s ease-out;box-shadow: 0px 0px 43px 0px rgba(131, 131, 131, 0.23);"><img style="width:120px;" src="https://truckerweb.herokuapp.com/assets/img/Logo_Trucker_Blanco_web.png"><p>Para completar tu registro, debes presentar en nuestras oficinas los siguientes documentos : <br>  '+names+' Recuerda que estamos para servirte!</p><a style="text-decoration: none;font-size: 14px;padding: 5px 14px;border-radius: 30px;font-family: sans-serif;letter-spacing: 1px;border: none; text-transform: uppercase;color: #e9c05e;">NOS COMUNICAREMOS CONTIGO</a></div><div style="margin-bottom:20px;width:100%;vertical-align: bottom;padding-bottom: 30px;background-color: #f7f7f7;"><img src="https://truckerweb.herokuapp.com/assets/img/Logo_Trucker_Blanco_web.png" style="width:57px;"><br>Cra 6 # 56 Calle Primera · Buenventura Valle<br> Centro de ayuda · Política de privacidad · Términos y condiciones<br> Dejar de recibir mensajes de este correo en '+email+' </div></div>'
  var mailOptions = {
    from: 'codebydevs@gmail.com',
    to: email,
    subject: 'Termina tu proceso de registro en TRUCKER ',
    html: htmlCustom,
  };

  transporter.sendMail(mailOptions, function(error, info){
    if (error) {
      console.log(error);
    } else {
      console.log('Email sent: ' + info.response);
    }
  }) 
}

exports.notificationRegisterPropietario =   async  function(email)
{

const htmlCustom = '<div style="width:auto;height:auto; background:#4466f4; color:white; padding:30px; margin:15px 0;text-align: center;-webkit-transition: background-image 0.3s ease-out;-moz-transition: background-image 0.3s ease-out;-o-transition: background-image 0.3s ease-out;transition: background-image 0.3s ease-out;box-shadow: 0px 0px 43px 0px rgba(131, 131, 131, 0.23);"><img style="width:120px;" src="https://truckerweb.herokuapp.com/assets/img/Logo_Trucker_Blanco_web.png"><p>Para completar tu registro, debes presentar en nuestras oficinas los siguientes documentos : <br>  Recuerda que estamos para servirte!</p><a style="text-decoration: none;font-size: 14px;padding: 5px 14px;border-radius: 30px;font-family: sans-serif;letter-spacing: 1px;border: none; text-transform: uppercase;color: #e9c05e;">NOS COMUNICAREMOS CONTIGO</a></div><div style="margin-bottom:20px;width:100%;vertical-align: bottom;padding-bottom: 30px;background-color: #f7f7f7;"><img src="https://truckerweb.herokuapp.com/assets/img/Logo_Trucker_Blanco_web.png" style="width:57px;"><br>Cra 6 # 56 Calle Primera · Buenventura Valle<br> Centro de ayuda · Política de privacidad · Términos y condiciones<br> Dejar de recibir mensajes de este correo en '+email+' </div></div>'
  var mailOptions = {
    from: 'codebydevs@gmail.com',
    to: email,
    subject: 'Notificación proceso de registro de propietartio TRUCKER ',
    html: htmlCustom,
  };

  transporter.sendMail(mailOptions, function(error, info){
    if (error) {
      console.log(error);
    } else {
      console.log('Email sent: ' + info.response);
    }
  }) 
}
