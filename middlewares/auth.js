const services = require('../services')
const TokenModel = require('../models/token')

function isAuth(req, res, next)
{
    if(!req.headers.authorization)
    {
       return res.status(403).send({ message: 'No tienes autorización'})
    }

    const token = req.headers.authorization
   
    var response = services.decodeToken(token)

    if( response.status == 200 )
      return next()
    

    return res.status(response.status).send({ response : response})
    next()
}

async function isLogged(req, res, next)
{
  const token = await  TokenModel.find({ 'token': req.params.token }).select("-_id")
  if(token.length >0)
    res.status(200).send({ status : true })
  else
  res.status(200).send({ status : false })
}


module.exports = 
{
    isAuth , 
    isLogged
}
 