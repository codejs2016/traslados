const mongoose = require('mongoose')
var Schema = mongoose.Schema;

const sedesSchema = new  Schema
({
    nombre : String,
    celular : String,
    ciudad : String,
    departamento: String,
    empresa : { type: String, trim: true },
    status : { type: Boolean , default : false },
    created_at : { type : Date , default : Date.now() } ,
})
module.exports = mongoose.model('sedes', sedesSchema )
