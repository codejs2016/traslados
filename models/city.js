const mongoose = require('mongoose')

const citySchema = new mongoose.Schema
({
    name : { type : String , unique : true, required: true  },
    status: { type: Boolean, default: true }
})
module.exports = mongoose.model('city', citySchema )
