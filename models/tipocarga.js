const mongoose = require('mongoose')

const TipoCargaSchema = new mongoose.Schema
({
    name : String ,
    status : { type: Boolean, default: true }
})
module.exports = mongoose.model('tipocarga', TipoCargaSchema )
