const mongoose = require('mongoose')

const PuertoSchema = new mongoose.Schema
({
    name : String ,
    status : { type: Boolean, default: true }, 
})
module.exports = mongoose.model('puerto', PuertoSchema )
