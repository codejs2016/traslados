const mongoose = require('mongoose')

const VehiclesSchema = new mongoose.Schema
({
    tipo : String ,
    status : { type: Boolean, default: true }, 
})
module.exports = mongoose.model('tipovehiculo', VehiclesSchema )
