const mongoose = require('mongoose')
var Schema = mongoose.Schema

const docSchema = new  Schema
({
    user: { type:mongoose.Schema.Types.ObjectId, ref:'users' } ,
    type: { type:mongoose.Schema.Types.ObjectId, ref:'typephoto' } ,
    documento : String,
    fechavencimiento: { type : Date , default : Date.now() },
    created_at : { type : Date , default : Date.now() } ,
})
module.exports = mongoose.model('docs', docSchema )
