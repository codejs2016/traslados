const mongoose = require('mongoose')
var Schema = mongoose.Schema;

const postulationSchema = new  Schema
({
    conductor : { type: Schema.Types.ObjectId, ref:'driver' },
    travel : { type: Schema.Types.ObjectId, ref:'travel' },
    status: { type: Boolean, default: true },
    created_at : { type : Date , default : Date.now() } ,
})
module.exports = mongoose.model('postulaciones', postulationSchema )
