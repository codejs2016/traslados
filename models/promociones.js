const mongoose = require('mongoose')

const PromocionesSchema = new mongoose.Schema
({
    name : String,
    imagen: String,
    status : { type: Boolean, default: true }, 
})
module.exports = mongoose.model('promociones', PromocionesSchema )
