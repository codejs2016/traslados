const mongoose = require('mongoose')

const ctgInspeccionSchema = new mongoose.Schema
({
    category : { type: String, required : true }, 
    expanded : { type: Boolean, default: true },
    observation:[],
    status: { type: Boolean, default: true }
})
module.exports = mongoose.model('ctginspeccion', ctgInspeccionSchema )
