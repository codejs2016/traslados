const mongoose = require('mongoose')
const bcrypt = require('bcrypt-nodejs')

var driverSchema = new mongoose.Schema
({
    names : String,
    surnames : String, 
    documentType : { type:mongoose.Schema.Types.ObjectId, ref:'documentType' },
    documentNumber : { type: String, unique : true, required : true, dropDups: true },
    address: String,
    phone:  { type: String, unique : true, required : true, dropDups: true }, 
    email : { type: String, unique : true, required : true, dropDups: true }, 
    password: String,
    username: String,
    gender: { type:mongoose.Schema.Types.ObjectId, ref:'genders' },
    city: String,
    status : { type: Boolean, default: true },
    role : { type:mongoose.Schema.Types.ObjectId, ref:'roles' },
    created_at : { type : Date , default : Date.now() }
})

driverSchema.pre('save' , function( next )
{
   let driver = this
   if( !driver.isModified('password')) 
     return next()
    
   bcrypt.genSalt(10, (err , salt) => 
   {
       if(err) return next(err)

       bcrypt.hash(driver.password, salt, null, (err, hash) => 
       {
           if(err) return next(err)

           driver.password = hash
           next()
       })
   })
})

module.exports =  mongoose.model('driver', driverSchema )
