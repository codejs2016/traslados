const mongoose = require('mongoose')

const colombiaSchema = new mongoose.Schema
({
    id: Number,
    departamento: String,
    ciudades: [ ]
})
module.exports = mongoose.model('dptcitycolombia', colombiaSchema )

