const mongoose = require('mongoose')
var Schema = mongoose.Schema;

const sedesSchema = new  Schema
({
    url : String,
    empresa : { type: String, trim: true },
    status : { type: Boolean , default : false }
})
module.exports = mongoose.model('ubicacion', sedesSchema )
