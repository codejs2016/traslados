const mongoose = require('mongoose')

const tallaSchema = new mongoose.Schema
({
    producto : { type : String, required: true  },
    talla: { type: String, require: true },
    cnt: { type: String, require: true }
})
module.exports = mongoose.model('tallaProdducto', tallaSchema )
