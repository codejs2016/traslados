const mongoose = require('mongoose')

var UserSchema = new mongoose.Schema
({
    puntoventa : String ,
    responsable : String ,
    producto : { type: String , lowercase: true } ,
    referencia: String,
    ctg : String ,
    cantidad: Number,
    vtotal:  Number ,
    status : { type : Boolean , default : true },
    created_at : { type : String } ,
    hora: String,
})

module.exports =  mongoose.model('traslado', UserSchema )