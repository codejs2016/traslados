const mongoose = require('mongoose')
const bcrypt = require('bcrypt-nodejs')

var UserSchema = new mongoose.Schema
({
    fullName : String ,
    surName : String ,
    documentType : String,
    documentNumber : { type: String,  dropDups: true },
    birthdate : Date ,
    address: String,
    phone:  String ,
    email : { type: String, lowercase: true, trim: true },
    gender: String,
    photo : String ,
    userName : { type: String, lowercase: true, trim: true },
    password : String ,
    status : { type: Boolean, default: true },
    role : { type:mongoose.Schema.Types.ObjectId, ref:'roles' } ,
    created_at : { type : Date , default : Date.now() } ,
    updated_at : Date,
    ciudad : String,
    codigo : String
})
UserSchema.pre('save' , function( next )
{
   let user = this
   if( !user.isModified('password')) 
     return next()
    
   bcrypt.genSalt(10, (err , salt) => 
   {
       if(err) return next(err)

       bcrypt.hash(user.password, salt, null, (err, hash) => 
       {
           if(err) return next(err)

           user.password = hash
           next()
       })
   })
})


module.exports =  mongoose.model('users', UserSchema )