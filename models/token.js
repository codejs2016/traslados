const mongoose = require('mongoose')

const tokenSchema = new mongoose.Schema
({
    token : String,
    company : String,
    status: { type: Boolean, default: true }
})
module.exports = mongoose.model('token', tokenSchema )
