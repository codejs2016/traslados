const mongoose = require('mongoose')

const waiterSchema = new mongoose.Schema
({
    name : String,
    type: Number,
    status: { type: Boolean }
})
module.exports = mongoose.model('typephoto', waiterSchema )
