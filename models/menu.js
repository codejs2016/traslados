const mongoose = require('mongoose')

const MenuSchema = new mongoose.Schema
({
    name : String ,
    action : String ,
    url : String ,
    type : Number,
    status : { type: Boolean, default: true }, 
})
module.exports = mongoose.model('menu', MenuSchema )
