const mongoose = require('mongoose')

const colorSchema = new mongoose.Schema
({
    name : { type: String, unique : true, required : true, dropDups: true }, 
    status: { type: Boolean, default: true }
})
module.exports = mongoose.model('colorvehicle', colorSchema )
