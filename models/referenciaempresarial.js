const mongoose = require('mongoose')

var refCompnySchema = new mongoose.Schema
({
    refcompany : { type:mongoose.Schema.Types.ObjectId, ref:'driver' },
    names : String ,
    surnames : String ,
    address:  { type: String, unique : true, required : true, dropDups: true },
    phone:   { type: String, unique : true, required : true, dropDups: true } ,
    city : { type: String },
    email :  { type: String, unique : true, required : true, dropDups: true },
    confirmacion: String
})
module.exports =  mongoose.model('refcompany', refCompnySchema )