const mongoose = require('mongoose')
var CvVehicleSchema = new mongoose.Schema
({
    plate:  { type: String, unique : true, required : true, dropDups: true }, 
    platetracker:  { type: String, unique : true, required : true, dropDups: true }, 
    modeloplate : { type: String },
    modeloplatetracker : { type: String },
    colorplate : { type: String },
    colorplatetracker : { type: String },
    kilometraje: { type: String },
    tipevehicle : { type: String },
    names : { type: String, lowercase: true },
    surnames :{ type: String, lowercase: true },
    documentType : { type:mongoose.Schema.Types.ObjectId, ref:'documentType' },
    documentNumber : { type: String, unique : true, required : true, dropDups: true }, 
    address: { type: String, lowercase: true },
    phone:  { type: String, unique : true, required : true, dropDups: true },
    email:  { type: String, unique : true, required : true, dropDups: true }, 
    gender: { type:mongoose.Schema.Types.ObjectId, ref:'genders' } ,
    city: { type: String, lowercase: true },
    status : { type: Boolean, default: true },
    role : { type:mongoose.Schema.Types.ObjectId, ref:'roles' } ,
    driver: { type:mongoose.Schema.Types.ObjectId, ref:'driver' } ,
    created_at : { type : Date , default : Date.now() }
})
module.exports =  mongoose.model('cvvehicle', CvVehicleSchema )
