const mongoose = require('mongoose')
var Schema = mongoose.Schema;

const productSchema = new  Schema
({
    name : { type: String,  dropDups: true },
    ctg: String,
    cnt: Number,
    stock: Number,
    vunit:  Number,
    vventa:  Number,
    tipoEmpaque: String,
    cntUnd: Number,
    vtotal: Number,
    ref: { type:String, lowercase:true },
    status: { type: Boolean, default: true },
    created_at : { type : Date , default : Date.now() } ,
    proveedor:  String,
    talla:  String,
    color:  String
})
module.exports = mongoose.model('product', productSchema )
