const mongoose = require('mongoose')

const waiterSchema = new mongoose.Schema
({
    name : String,
    status: { type: Boolean, default: true }
})
module.exports = mongoose.model('departamento', waiterSchema )
