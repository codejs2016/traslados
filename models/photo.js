const mongoose = require('mongoose')
var Schema = mongoose.Schema;

const vehicleSchema = new  Schema
({
    plate: { type:mongoose.Schema.Types.ObjectId, ref:'cvvehicle' } ,
    typephoto: { type:mongoose.Schema.Types.ObjectId, ref:'typephoto' } ,
    photo : String,
    created_at : { type : Date , default : Date.now() } ,
})
module.exports = mongoose.model('photovehicle', vehicleSchema )
