const mongoose = require('mongoose')

const storageInspeccionSchema = new mongoose.Schema
({
    vehiculo: { type:mongoose.Schema.Types.ObjectId, ref:'cvvehicle' } ,
    category : { type: String, required : true }, 
    observation:[],
    status: { type: Boolean, default: true }
})
module.exports = mongoose.model('storageinspeccion', storageInspeccionSchema )
