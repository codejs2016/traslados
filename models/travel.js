const mongoose = require('mongoose')
var Schema = mongoose.Schema;

const registerTravelSchema = new  Schema
({
    company : { type: Schema.Types.ObjectId, ref:'company' },
    cityOrigen : String,
    cityDestination : String, 
    port : String,
    bodega : String,
    typelaod : String,
    vigencia : String,
    typevhicle : String ,
    weight : { type: String },
    flete : { type: String },
    status: { type: Boolean, default: true },
    created_at : { type : Date , default : Date.now() } ,
    updated_at : Date 
})
module.exports = mongoose.model('travel', registerTravelSchema )
