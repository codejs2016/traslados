const mongoose = require('mongoose')

const companySchema = new mongoose.Schema
({
    celular: String ,
    telefono: String,
    indicactivo: String,
    contacto: String,
    correo: String,
    direccion: String,
    nit: { type:String, trim: true },
    digito: String,
    ubicaciongps: String,
    img : String,
    ccomercio: String,
    rut: String,
    phone : String,
    dpt: String,
    city: String,
    role: { type:mongoose.Schema.Types.ObjectId, ref:'role' } ,
    businessname: String,
    status: { type: Boolean, default: false } ,
    password :  { type: String  }, 
    representantelegal : String , 
    rutname : String , 
    ccomercio : String
})
module.exports = mongoose.model('company', companySchema )
